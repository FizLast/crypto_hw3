/** @type import('hardhat/config').HardhatUserConfig */
require("@nomicfoundation/hardhat-toolbox");
require('dotenv').config();


module.exports = {
  solidity: "0.8.20",
  paths: {
    sources: "./contracts",
  },
  networks: {
    goerli: {
      url: process.env.ALCHEMY_URL_GOERLI,
      accounts: [process.env.WALLET_PRIVATE_KEY]
    }
  }
};