async function main() {
  const [deployer] = await ethers.getSigners();
  
  const HWToken = await ethers.getContractFactory('HWToken', deployer)
  const hwToken = await HWToken.deploy();

  console.log("PN288 contract is ", hwToken)
}

main()
  .then(() => process.exit(0))
  .catch(() => {
    console.error(error);
    process.exit(1);
  });