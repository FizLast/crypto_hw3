const { ethers } = require('ethers');
const { BigNumber } = require("@ethersproject/bignumber");
const axios = require('axios');
require('dotenv').config();

async function createPool() {
    try {
        const factoryUrl = `https://api.etherscan.io/api?module=contract&action=getabi&address=${process.env.UNISWAP_FACTORY_ADDRESS}&apikey=${process.env.ETHERSCAN_API_KEY}`
        const factoryRes = await axios.get(factoryUrl)
        const factoryABI = JSON.parse(factoryRes.data.result)
        
        console.log("Connecting to Ethereum provider...");
        const provider = new ethers.JsonRpcProvider(process.env.ALCHEMY_URL_GOERLI);

        console.log("Creating wallet...");
        const wallet = new ethers.Wallet(process.env.WALLET_PRIVATE_KEY, provider);

        console.log("Deploying UniswapV3Factory");
        const uniswapFactory = new ethers.Contract(process.env.UNISWAP_FACTORY_ADDRESS, factoryABI, wallet);

        console.log("Creating a new pool");
        const POOL_FEE = 3000;
        const PN288_ADDRESS = process.env.MYTOKEN_ADDRESS;
        const WETH_ADDRESS = process.env.WETH_ADDRESS;

        const tx = await uniswapFactory.createPool(PN288_ADDRESS, WETH_ADDRESS, POOL_FEE, { gasLimit: 10000000 });
        const receipt = await tx.wait();
        console.log(receipt);

        const uniswapPoolAddress = await uniswapFactory.getPool(PN288_ADDRESS, WETH_ADDRESS, POOL_FEE);
        console.log("Address of the new pool: " + uniswapPoolAddress);
    } catch (error) {
        console.error(error);
    }
}

createPool()
.then(() => process.exit(0))
.catch(() => {
  console.error(error);
  process.exit(1);
});
