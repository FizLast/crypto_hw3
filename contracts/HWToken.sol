pragma solidity ^0.8.20;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract HWToken is ERC20 {
    constructor() ERC20('PN288', 'PN288 Token') {
        _mint(msg.sender, 1000 * 10 ** 18);
    }
}